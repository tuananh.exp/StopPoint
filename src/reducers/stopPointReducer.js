import {
    GET_STOP_POINT_REQUEST,
    GET_STOP_POINT_SUCCESS,
    GET_STOP_POINT_FAILURE,
} from '../actions';

const initialState = {
    stopPoint: null,
    stopPointGetting: false,
    stopPointGettingError: false
};

function initializeState() {
    return Object.assign({}, initialState);
}

export default function stopPointReducer(state = initializeState(), action = {}) {
    switch (action.type) {

        case GET_STOP_POINT_REQUEST:
            return {
                ...state,
                stopPoint: null,
                stopPointGetting: true,
                stopPointGettingError: false
            };

        case GET_STOP_POINT_SUCCESS:
            return {
                ...state,
                stopPoint: action.data.stopPointSequences[0],
                stopPointGetting: false
            };

        case GET_STOP_POINT_FAILURE:
            return {
                ...state,
                stopPointGettingError: action.error,
                stopPointGetting: false
            };

        default:
            return state;
    }
}