import React from "react";
import { Platform, StatusBar } from 'react-native';
import { Provider } from "react-redux";
import { Actions } from 'react-native-router-flux';

import store from "../store";
import Routes from '../routes';

class Root extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <Routes />
      </Provider>
    );
  }
}

export default Root;