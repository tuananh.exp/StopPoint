import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  ListView,
  Platform,
  RefreshControl,
  Dimensions,
} from 'react-native';
import React, { Component, PropTypes }  from 'react';
import { bindActionCreators }           from 'redux';
import { connect }                      from 'react-redux';
import { Actions, ActionConst }         from "react-native-router-flux";
import Icon                             from 'react-native-vector-icons/FontAwesome';
import {
  getStopPointAction,
  getBusArrivalsAction
}                                       from '../actions';
import Spinner                          from 'react-native-loading-spinner-overlay';
import { STYLE }                        from '../utils/variables';
const {width, height} = Dimensions.get('window');

class StopPoint extends Component {

    constructor(props) {
      super(props);
      const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
      this.state = {
        refreshing: false,
        dataSource: ds.cloneWithRows(this.props.stopPointReducer.stopPoint.stopPoint)
      };
    }

    componentDidMount() {
        
    }
    
     componentWillReceiveProps(nextProps) {
        if (nextProps.busArrivalsReducer.busArrivals && nextProps.busArrivalsReducer.busArrivals !== this.props.busArrivalsReducer.busArrivals) {
            Actions['busArrivals']({ type: ActionConst.PUSH });
        }
    }
  
     _onSelectedPoint(selectedPointId) {
       console.log(this.props);
      if (selectedPointId) {
        this.props.getBusArrivalsAction(selectedPointId);
      }
    }

    _onRefresh() {
      this.props.getStopPointAction(this.props.stopPointReducer.stopPoint.lineId);
    }
  
    _renderRow(data) {
      return (
        <TouchableOpacity onPress={() => this._onSelectedPoint(data.id)}>
          <View style={styles.row}>
            <View style={styles.description}>
              <View style={styles.descriptionRow1}>
                <Icon name='circle' color={data.status ? STYLE.green : 'red'} size={15} style={styles.iconDirect}/>
                <Text style={styles.text}>{data.name}</Text>
              </View>
              <View style={styles.descriptionRow2}>
                <Icon name='long-arrow-right' color='#ddd' size={20} style={styles.iconDirect} />
                <Text style={styles.text}>
                 {data.towards || 'No stop point'}
                </Text>
              </View>  
            </View>
            <Icon name='angle-right' color='#000' size={20} style={styles.iconReadMore} />
          </View>
        </TouchableOpacity>
      )
    }

    render() {
        return (
          <View style={styles.container}>
              <Spinner visible={this.props.busArrivalsReducer.busArrivalsGetting} />
              <View style={styles.header}>
                {
                  this.props.stopPointReducer.stopPoint && this.props.stopPointReducer.stopPoint.lineId &&
                  <Text style={styles.headerText}>
                    Bus Number {this.props.stopPointReducer.stopPoint.lineId}
                  </Text>
                }
              </View>
              <ListView
                dataSource={this.state.dataSource}
                renderRow={(rowData) => this._renderRow(rowData) }
                refreshControl={
                  <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={() => this._onRefresh()}
                  />
                }
              />
            </View>
    )
  }
}

StopPoint.propTypes = {
  getStopPointAction: PropTypes.func,
  getBusArrivalsAction: PropTypes.func
}

const stateToProps = (state) => {
  return {
    stopPointReducer: state.stopPointReducer,
    busArrivalsReducer: state.busArrivalsReducer
  }
}

const dispatchToProps = (dispatch) => {
  return bindActionCreators({getStopPointAction, getBusArrivalsAction}, dispatch)
}

const styles = StyleSheet.create({
  container: {
    marginTop: 60,
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#F5FCFF'
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    borderColor: '#ddd',
    borderBottomWidth: 1,
    marginVertical: 3,
    backgroundColor: STYLE.white
  },
  description: {
    flex: 1,
    flexDirection: 'column',
    paddingHorizontal: 10,
  },
  text: {
    flex: 0.6,
    fontSize: 14,
    marginBottom: 5,
  },
  textSub: {
    flex: 0.6,
    fontSize: 12
  },
  iconReadMore: {
    width: 20,
    height: 30,
  },
  iconDirect: {
    width: 40,
    paddingLeft: 10,
  },
  descriptionRow1: {
    flexDirection: 'row',
  },
  descriptionRow2: {
    flexDirection: 'row',
  },
  header: {
    flex: 0.1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  headerText: {
    color: STYLE.blue,
    fontSize: 20,
    fontWeight: '300'
  }
})

export default connect(stateToProps, dispatchToProps)(StopPoint)