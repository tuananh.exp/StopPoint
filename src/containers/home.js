import {
  View,
  Text,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  Image,
  ScrollView,
  Platform,
  Dimensions,
}                                       from 'react-native';
import React, { Component, PropTypes }  from 'react';
import { bindActionCreators }           from 'redux';
import { connect }                      from 'react-redux';
import { Actions, ActionConst }         from "react-native-router-flux";
import Icon                             from 'react-native-vector-icons/FontAwesome';
import { getStopPointAction }           from '../actions';
import { STYLE }                        from '../utils/variables';
import Spinner                          from 'react-native-loading-spinner-overlay';

const {width, height} = Dimensions.get('window');

class Home extends Component {

    constructor(props) {
        super(props);
        this.state = { id: null };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.stopPointReducer.stopPoint && nextProps.stopPointReducer.stopPoint !== this.props.stopPointReducer.stopPoint) {
            Actions['stopPoint']({ type: ActionConst.PUSH });
        }
    }

    _onSubmitId() {
        if(this.state.id) {
            this.props.getStopPointAction(this.state.id);
        }
    }

    scrolldown() {
        this.refs.scrollView.scrollTo({y: 50, animated: true});
    }

    render() {
        return (
            <View style={[styles.container, styles.bg]}>
                <Spinner visible={this.props.stopPointReducer.stopPointGetting} />
                <ScrollView contentContainerStyle={styles.content}   scrollEnabled={false} ref="scrollView">
                    <View style={styles.iconBg}>
                        <Icon name='bus' size={40} style={styles.icon}/>
                    </View>
                    <Text style={styles.headerText}>Bus Station</Text>
                    <View style={styles.inputContainer}>
                        <Text style={styles.contentText}>Bus Number</Text>
                        <TextInput
                            autoFocus={true}
                            style={styles.contentTextInput}
                            onChangeText={(id) => this.setState({ id })}
                            value={this.state.id}
                            keyboardType={'numeric'}
                        />
                    </View>
                    <Text style={styles.contentError}>
                        {this.props.stopPointReducer.stopPointGettingError ? 'Not Found' : ''}
                    </Text>
                </ScrollView>
                <TouchableOpacity style={styles.submit} onPress={this._onSubmitId.bind(this)}>
                    <Icon name='search' size={20} style={styles.iconSearch}/>
                    <Text style={styles.textSubmit}> Search </Text>
                </TouchableOpacity>
            </View>
    );
  }
}

Home.propTypes = {
    getStopPointAction: PropTypes.func,
    stopPoint: PropTypes.object
}

const stateToProps = (state) => {
  return {
      stopPointReducer: state.stopPointReducer
  }
}

const dispatchToProps = (dispatch) => {
  return bindActionCreators({ getStopPointAction }, dispatch)
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
    },
    content: {
        flex: 1,
        height,
        marginTop: 0,
        backgroundColor: STYLE.header,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },
    iconBg: {
        backgroundColor: STYLE.white,
        padding: 20,
        borderRadius: 50,
        marginBottom: 10
    },
    icon: {
        color: STYLE.header
    },
    bg: {
        backgroundColor: STYLE.blue,
    },
    headerText: {
        color: STYLE.white,
        fontSize: 30,
        fontWeight: "100",
        marginTop: 20,
        marginBottom: 50
    },
    inputContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    submit: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        width: width,
        backgroundColor: STYLE.greenDarker,
    },
    textSubmit: {
        color: STYLE.white,
        fontSize: 20,
        padding: 20,
        textAlign: 'center'
    },
    contentText: {
        // marginBottom: 20,
        fontSize: 16,
        color: STYLE.white,
        alignItems: 'center',
        justifyContent: 'center'
    },
    contentTextInput: {
        height: 40,
        width: 100,
        backgroundColor: STYLE.greyLight,
        marginHorizontal: 20,
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
    iconSearch: {
        width: 20,
        color: STYLE.white
    },
    contentError: {
        paddingVertical: 20,
        color: 'red',
        fontSize: 16,
    }
})

export default connect(stateToProps, dispatchToProps)(Home)